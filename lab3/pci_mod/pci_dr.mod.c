#include <linux/build-salt.h>
#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

BUILD_SALT;

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__section(.gnu.linkonce.this_module) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used __section(__versions) = {
	{ 0xdd8f8694, "module_layout" },
	{ 0x6bc3fbc0, "__unregister_chrdev" },
	{ 0x2eba3d32, "pci_unregister_driver" },
	{ 0x9a19ed29, "__register_chrdev" },
	{ 0x9c70ff83, "__pci_register_driver" },
	{ 0xd9a5ea54, "__init_waitqueue_head" },
	{ 0x93a219c, "ioremap_nocache" },
	{ 0x53774886, "pci_request_region" },
	{ 0xdef7c04a, "pci_enable_device" },
	{ 0xa689370e, "pci_select_bars" },
	{ 0x6091b333, "unregister_chrdev_region" },
	{ 0x37879d4d, "pci_disable_device" },
	{ 0xd87af866, "pci_release_regions" },
	{ 0xdecd0b29, "__stack_chk_fail" },
	{ 0x92540fbf, "finish_wait" },
	{ 0x8c26d495, "prepare_to_wait_event" },
	{ 0x1000e51, "schedule" },
	{ 0xfe487975, "init_wait_entry" },
	{ 0xa1c76e0a, "_cond_resched" },
	{ 0x26948d96, "copy_user_enhanced_fast_string" },
	{ 0xafb8c6ff, "copy_user_generic_string" },
	{ 0x72a98fdb, "copy_user_generic_unrolled" },
	{ 0xb44ad4b3, "_copy_to_user" },
	{ 0xc5850110, "printk" },
	{ 0xbdfb6dbb, "__fentry__" },
};

MODULE_INFO(depends, "");

MODULE_ALIAS("pci:v00008086d00002723sv*sd*bc*sc*i*");

MODULE_INFO(srcversion, "21738CEC2CB098A5B016F67");
