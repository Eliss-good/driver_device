#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>

#include "./header/comm.h"

long long ioctl_get_mac (int fd) 
{		
	return ioctl(fd, RD_MAC);
}


int main() 
{ 
	long long mac;
    int fd = open("/dev/pci_ioctl_device", O_RDWR);
    if (fd == -1) {
        perror("Не удалось открыть файл \nОшибка");
        return 1;
    }

    mac = ioctl_get_mac (fd);

	printf("%x\n", mac);

    close(fd);
    return 0;
}
